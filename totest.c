MODULE SetMotionMode
    
    PROC main()
        CONST num testing:=200;
        VAR speeddata speed:=[testing,testing,testing,testing];
        MoveToCalibPos;
        MoveToTablePos;
        setMode 2,speed,0,0,100;
        setMode 1,speed,300,0,300;
        TurnVacOn;
        WaitTime 2;
        setMode 1,speed,0,0,100;
        TurnVacOff;
        setMode 1,speed,300,0,200;
        TurnVacOn;
        WaitTime 2;
        setMode 2,speed,100,0,100;
        TurnVacOff;
        setMode 1,speed,200,0,400;
        setMode 1,speed,400,0,50;
        setMode 1,speed,50,0,50;
        setMode 1,speed,50,0,50;
        MoveToTablePos;
        MoveToCalibPos;
        
    ENDPROC
    
    
    
    PROC setMode(num mode,speeddata speedSelect,num xpos, num ypos, num zpos)
        IF mode=1 THEN !linear mode
            MoveL Offs(pTableHome,xpos,ypos,zpos),speedSelect,fine,tSCup;
        ELSEIF mode=2 THEN !joint mode
            MoveJ Offs(pTableHome,xpos,ypos,zpos),speedSelect,fine,tSCup;
            !MoveJ Offs(pTableHome,150,150,5),speedSelect,fine,tSCup;
        ENDIF
    ENDPROC
    
    PROC MoveToConveyorPos()
        CONST jointtarget jtCalibPos:=[[90,0,0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
        MoveAbsJ jtCalibPos,v400,fine,tool0;
    ENDPROC
    
    PROC MoveToTablePos()
        CONST jointtarget jtCalibPos:=[[0,0,0,0,0,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
        MoveAbsJ jtCalibPos,v400,fine,tool0;
    ENDPROC
    
ENDMODULE