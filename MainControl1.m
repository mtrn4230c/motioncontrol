% GUI for the main control of the robot.
% Authors: Amir, Imran, Jackson, Steven, Thoby and Vihaan
% Date modified: 13/09/17

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Change Log:
% 04/09 - GUI Figure with create and callback - Vihaan
% 06/09 - Comms protocol - Imran, Jackson and Vihaan
% 09/09 - GUI Figure with create and callback update - Vihaan
% 10/09 - Video feed - Amir
% 10/09 - Click and go - Steven
% 11/09 - TCP/IP StartSockets() SendCommand(), Opening and Delete Fcns- Imran
% 11/09 - Timer: StartTimer() - Imran
% 11/09 - Qwirkle - Amir
% 11/09 - move robot - Steven
% 12/09 - Advise commands: CommuincationBox() - Imran
% 12/09 - Video fixed: StartCameras(), PlotButtonDown() - Imran
% 12/09 - receive from robot studio: ReceiveCountCallback() - Vihaan 
% 12/09 - I/O control - Imran
% 12/09 - Lindt - Amir
% 12/09 - Manual jogging - Imran
% 12/09 - Set pose update - Thoby (xyz set) and Imran (speed, mode)
% 12/09 - run and pause - Thoby
% 13/09 - qwirkle update - Vihaan
% 13/09 - ClickToPose update - Imran

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Communication protocol:
% The following are valid headers:
% PO -> Robot Pose (x, y, z) [Receiving only]
% Ex: PO[x,y,z] (where x, y, and z are the coordinates of the robot end
% effector)

% QN -> Robot Pose (q1, q2, q3, q4) [Receiving only]
% Ex: QNq1,q2,q3,q4 (where q1, q2, q3, and q4 are the quaternions
% representing the angular pose of the end effector)

% IO -> I/O (followed by the output number).  [Sending and Receiving]
% Ex: IO2 (set output 2) [Sending]
% Ex: IOO1,O2,O3,O4,I1 (O1, O2, O3, and O4 are outputs and I1 is the input)
% [Receiving]

% MJ -> Move in joint mode. [Sending only]
% Ex: MJ[x,y,z] (where x, y, and z are the coordinates of the robot end
% effector)

% ML -> Move in linear mode. [Sending only]
% Ex: ML[x,y,z] (where x, y, and z are the coordinates of the robot end
% effector)

% SP -> Set the speed of the robot. [Sending only]
% SPs (where s is the speed as a percentage of the max speed)

% LJ -> Linear jog. [Sending only]
% Ex: LJA+ (where A is the axis along which the motion occurs and + is the
% direction of motion)

% JJ -> Joint jog. [Sending only]
% Ex: JJx+0 (where x is the joint number, + indicates the direction of
% rotation and 0 indicates that the increment is of size 10)

% LI -> Linear increment size. [Sending only]
% Ex: LI010 (The last 3 digits indicate the size of the increment)

% PM -> Pause motion. [Sending only]
% Ex: PM

% CM -> Continue motion. [Sending only]
% Ex: CM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function varargout = MainControl(varargin)
% MAINCONTROL MATLAB code for MainControl.fig
%      MAINCONTROL, by itself, creates a new MAINCONTROL or raises the existing
%      singleton*.
%
%      H = MAINCONTROL returns the handle to a new MAINCONTROL or the handle to
%      the existing singleton*.
%
%      MAINCONTROL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAINCONTROL.M with the given input arguments.
%
%      MAINCONTROL('Property','Value',...) creates a new MAINCONTROL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MainControl_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MainControl_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MainControl

% Last Modified by GUIDE v2.5 27-Sep-2017 15:53:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @MainControl_OpeningFcn, ...
    'gui_OutputFcn',  @MainControl_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%% MainControl StartUp and Delete



function MainControl_OpeningFcn(hObject, eventdata, handles, varargin)
% --- Executes just before MainControl is made visible.
% Set up the sockets for communication and the timer for handling
% interrupts.
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MainControl (see VARARGIN)

% Choose default command line output for MainControl
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Clear global variables
clearvars -global

% Set global variable for number of times MainControl has attempted
% to read from the receive socket
global ReceiveCount;
ReceiveCount = 0;

global motionMode;
motionMode = 'J';

global Joint;
Joint = 'JJ1';

global QwirkleInfo;
QwirkleInfo = [26, 26, 0, 0, 0, 0, 0];

% Start logs, sockets, timer, and camera
StartCommunicationBox(handles);
StartSockets(handles);
StartTimer(handles);
StartCameras(handles);
StartQwirkleTimer(handles);

% UIWAIT makes MainControl wait for user response (see UIRESUME)
% uiwait(handles.figure1);



function varargout = MainControl_OutputFcn(hObject, eventdata, handles)
% --- Outputs from this function are returned to the command line.
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function MainControl_DeleteFcn(hObject, eventdata, handles)
% --- Executes when objects are deleted
% Stop the timer and close the socket connections.
% This function has no inputs or outputs.
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ReceiveCount;
global SendSocket;
global ReceiveSocket;

% stop and delete timer
stop(ReceiveCount);
delete(ReceiveCount);



% close sockets
fclose(SendSocket);
fclose(ReceiveSocket);

close;


function figure1_DeleteFcn(hObject, eventdata, handles)
% --- Executes when objects are deleted
% Stop the timer and close the socket connections.
% This function has no inputs or outputs.
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ReceiveCount;
global SendSocket;
global ReceiveSocket;

% stop and delete timer
stop(ReceiveCount);
delete(ReceiveCount);

% close sockets
fclose(SendSocket);
fclose(ReceiveSocket);



%% Communication Box

function StartCommunicationBox(handles)
% Initialise the box for logging instructions sent to and received from
% RAPID.
% The input to this function is the handles object.clear ('communicationBox');
global communicationBox;
communicationBox{1} = 'Started GUI';
set(handles.communicationBox, 'string', communicationBox);


function communicationBox(handles, text)
% Function for updating the communication box with messages.
% This function takes the handles object and the message string as the
% input, and has no output.
global communicationBox;
communicationBox = [{text}, communicationBox];
set(handles.communicationBox, 'string', communicationBox);


function communicationBox_Callback(hObject, eventdata, handles)
% --- Executes on selection change in communicationBox.
% This function takes in the object being changed, the event data and the
% handles object as the inputs and has no output.
% hObject    handle to communicationBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns communicationBox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from communicationBox



function communicationBox_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to communicationBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0, ...
        'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% Sockets

function StartSockets(handles)
% Initiliase the sockets.
% This function takes in the handles object as an input, and has no output.
global SendSocket;
global ReceiveSocket;

% CHANGE THIS FOR REAL ROBOT / ACTUAL ROBOT
% robot_IP_address = '192.168.125.1';
robot_IP_address = '127.0.0.1';

% send socket
send_port = 1025;
SendSocket = tcpip(robot_IP_address, send_port);
set(SendSocket, 'ReadAsyncMode', 'continuous', 'Tag', 'SendSocket');
% fopen(SendSocket);

% open send socket
communicationBox(handles, 'Connecting to the send socket...');
try
    fopen(SendSocket);
    if(~IsSocketOpen(SendSocket))
        msgID = 'OpenSendSocket:SocketNotOpen';
        msg = 'Failed to open TCP connection to send socket';
        e = MException(msgID,msg);
        throw(e);
    end
    communicationBox(handles, 'Opened send socket');
catch ME
    switch ME.message
        case 'Unsuccessful open: OBJ has already been opened.'
            return;
        otherwise
            communicationBox(handles, 'Unable to open send socket');
            pause(1);
    end
end



% receive socket
receive_port = 1026;
ReceiveSocket = tcpip(robot_IP_address, receive_port, 'Timeout', 0.1);
set(ReceiveSocket, 'ReadAsyncMode', 'continuous', 'Tag', ...
    'ReceiveSocket');
% fopen(ReceiveSocket);

% open receive socket
communicationBox(handles, 'Connecting to the receive socket...');
try
    fopen(ReceiveSocket);
    if(~IsSocketOpen(ReceiveSocket))
        msgID = 'OpenReceiveSocket:SocketNotOpen';
        msg = 'Failed to open TCP connection to receive socket';
        e = MException(msgID,msg);
        throw(e);
    end
    communicationBox(handles, 'Opened receive socket');
catch ME
    switch ME.message
        case 'Unsuccessful open: OBJ has already been opened.'
            return;
        otherwise
            communicationBox(handles, 'Unable to open receive socket');
            pause(1);
    end
end

% Check if socket open
function isopen = IsSocketOpen(socket)
isopen = isequal (get(socket, 'Status'), 'open');


function SendCommand(text)
% This function sends a string to send socket.
% The input to this function is the command to send. The function assumes
% that the command being set obeys the communication protocol. There is no
% output.
global SendSocket;
disp(text);
fwrite (SendSocket, text);


%% Timer

function StartTimer(handles)
% This function creates and starts the timer. It takes in the handles
% object as the input and has no outputs.
global ReceiveCount;
ReceiveCount = timer('ExecutionMode', 'fixedSpacing', 'Period',...
    0.1, 'StartDelay',2,'TimerFcn',{@ReceiveCountCallback, handles});
ReceiveCount.TimerFcn = {@ReceiveCountCallback, handles};
start(ReceiveCount);


function ReceiveCountCallback(hObject, eventdata, handles)
% This is the callback function from the timer. It checks whether data has
% been received from RAPID. If data has been received, the function parses
% it according to the communication protocol and updates the values
% accordingly.
% The inputs to this function are the object raising the event, the event
% data, and the handles object. There is no output.
global ReceiveCount;
global ReceiveSocket;

try
    data = fgetl(ReceiveSocket);
    % Clear buffer after this
    if isempty(data)
        error('No data');
    end
    ReceiveCount = 0;
catch ME
    ReceiveCount = ReceiveCount + 1;
    
    % more than 15 errors in a row --> diplay message
    if ReceiveCount > 15
        communicationBox(handles, ...
            'Connection to Receive Socket Lost');
    end
    return;
end


% Display the message data in the communication box
lastCharIdx = length(char(data));
if lastCharIdx > 2
    dataType = char(data(1:2));
    switch dataType
        case 'PO'
%             global posReceived;
            pose = char(data(3:lastCharIdx));
            posReceived = str2num(pose);
            set(handles.xValue, 'string', ...
                ['x = ' num2str(posReceived(1))]);
            set(handles.yValue, 'string', ...
                ['y = ' num2str(posReceived(2))]);
            set(handles.zValue, 'string', ...
                ['z = ' num2str(posReceived(3))]);
            
        case 'QN'
%             global angReceived;
            ang = char(data(3:lastCharIdx));
            angReceived = str2num(ang);
            set(handles.q1Value, 'string', ...
                ['q1 = ' num2str(angReceived(1))]);
            set(handles.q2Value, 'string', ...
                ['q2 = ' num2str(angReceived(2))]);
            set(handles.q3Value, 'string', ...
                ['q3 = ' num2str(angReceived(3))]);
            set(handles.q4Value, 'string', ...
                ['q4 = ' num2str(angReceived(4))]);
            
        case 'IO'
            global IOReceived;
            io = char(data(3:lastCharIdx));
            IOReceived = str2num(io(1:end-1));
            %disp(IOReceived);
            %             IOReceived = str2num(io);
            set(handles.conStatValue, 'string', ...
                ['Con Stat = ' num2str(IOReceived(5))]);
            if IOReceived(1) == 0
                set(handles.vacpump, 'string', 'Vac Pump OFF');
                IOReceived(5) = 1;
                set(handles.conStatValue, 'string', ...
                    ['Con Stat = ' num2str(IOReceived(5))]);
            else
                set(handles.vacpump, 'string', 'Vac Pump ON');
            end
            if IOReceived(2) == 0
                set(handles.vacsol, 'string', 'Vac Sol OFF');
            else
                set(handles.vacsol, 'string', 'Vac Sol ON');
            end
            if IOReceived(4) == 0
                set(handles.condir, 'string', 'Con Dir Tow');
            else
                set(handles.condir, 'string', 'Con Dir Away');
            end
            if IOReceived(5) == 0 || IOReceived(3) == 0
                set(handles.conrun, 'string', 'Con Run OFF');
                if IOReceived(5) == 0
                    set(handles.conrun, 'enable', 'off');
                else
                    set(handles.conrun, 'enable', 'on');
                end
            else
                set(handles.conrun, 'string', 'Con Run ON');
            end
            
            % Clear any additional messages in the socket
            flushinput(ReceiveSocket);
            
        otherwise
            communicationBox(handles, data);
    end
end

%% Timer for updating Qwirkle block detection
function StartQwirkleTimer(handles)
% This function creates and starts the timer for qwirkle block detection.
% It takes in the handles object as the input and has no outputs.
% The timer calls the callback function every 2 seconds.
global QwirkleTimer;
QwirkleTimer = timer('ExecutionMode', 'fixedSpacing', 'Period',...
    2, 'StartDelay',2,'TimerFcn',{@QwirkleBlockDetection, handles});
QwirkleTimer.TimerFcn = {@QwirkleBlockDetection, handles};
start(QwirkleTimer);

%% Cameras
function StartCameras(handles)
% Initialise the camera feeds.
% This function takes the handles object as the input and has no output.
global TableVidFeed;
global ConveyorVidFeed;
global QwirkleInfo;

try
    % connect to table video feed
    axes(handles.TablePlot);
    TableVidFeed =  videoinput('winvideo', 1, 'RGB24_1600x1200');
    set(TableVidFeed, 'ReturnedColorSpace', 'rgb');
    TableVidRes = get(TableVidFeed, 'VideoResolution');
    TableBandNo = get(TableVidFeed, 'NumberOfBands');
    TableImage = image(zeros(TableVidRes(2), TableVidRes(1), ...
        TableBandNo));
    preview(TableVidFeed, showColourShape(QwirkleInfo, TableImage));
    
    % connect to conveyor video feed
    axes(handles.ConveyorPlot);
    ConveyorVidFeed =  videoinput('winvideo', 2, 'RGB24_1600x1200');
    set(ConveyorVidFeed, 'ReturnedColorSpace', 'rgb');
    ConveyorVidRes = get(ConveyorVidFeed, 'VideoResolution');
    ConveyorBandNo = get(ConveyorVidFeed, 'NumberOfBands');
    ConveyorImage = image(zeros(ConveyorVidRes(2), ...
        ConveyorVidRes(1), ConveyorBandNo));
    preview(ConveyorVidFeed, ConveyorImage);
catch
end


function TablePlot_ButtonDownFcn(hObject, eventdata, handles)
% --- Executes on table plot mouse press
% This function sends the command to move the robot to the location of the
% mouse click. This is a callback function when the mouse is pressed.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
CameraID = 0;
Point = get(hObject, 'CurrentPoint');
Point = Point (1,1:2);
Point(2) = 1 - Point(2);
position = ClickToPose(Point, CameraID);
global motionMode;
PointString = sprintf(['M' motionMode '[%s,%s,%s]'], ...
    num2str(position(1),'%.2f'), ...
    num2str(position(2),'%.2f'), ...
    num2str(position(3),'%.2f'));
SendCommand(PointString);




function ConveyorPlot_ButtonDownFcn(hObject, eventdata, handles)
% --- Executes on conveyor plot mouse press
% This function sends the command to move the robot to the location of the
% mouse click. This is a callback function when the mouse is pressed.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
CameraID = 1;
Point = get(hObject, 'CurrentPoint');
Point = Point (1,1:2);
Point(2) = 1 - Point(2);
position = ClickToPose(Point, CameraID);
global motionMode;
PointString = sprintf(['M' motionMode '[%s,%s,%s]'], ...
    num2str(position(1),'%.2f'), ...
    num2str(position(2),'%.2f'), ...
    num2str(position(3),'%.2f'));
SendCommand(PointString);



function position = ClickToPose(Point,camera)
% This function evaluates the point in the Robot's world frame that
% corresponds to the location of the mouse click.
% The inputs to this function are the point and the camera feed. The output
% is the point in the converted coordinate frame.
if size(Point,2) == 1
    Point = Point';
end
Point(1) = Point(1) * 1600;
Point(2) = Point(2) * 1200;
position = zeros(1,3);
if camera == 0
    try
        load('cameraParamsTable.mat');
    catch
    end
    parameters = cameraParamsTable;
    offset = [175 0 157];
    centre = [799 288];
    angle = 0.5;
else
    try
        load('cameraParamsConveyor.mat');
    catch
    end
    parameters = cameraParamsConveyor;
    offset = [175 0 157];
    centre = [219 780];
    angle = 1;
end
angle = -deg2rad(angle);
A = [cos(angle) -sin(angle) centre(1)-cos(angle)*centre(1)+sin(angle)*centre(2); ...
    sin(angle) cos(angle) centre(2)-sin(angle)*centre(1)-cos(angle)*centre(2); ...
    0 0 1];
Point = undistortPoints(Point, parameters);
Point(3) = 1;
Point = A*Point';
scale = 1.525;

position(1:2) = [(Point(2)-centre(2))/scale (Point(1)-centre(1))/scale];

position = [position(1:2)+offset(1:2) offset(3)];

return



function runButton_Callback(hObject, eventdata, handles)
% --- Executes on button press in runButton.
% This function sends the command to continue robot motion.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to runButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SendCommand('CM');



function JointMode_Callback(hObject, eventdata, handles)
% --- Executes on button press in JointMode.
% This function sends a message to move the robot in joint mode.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to JointMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of JointMode
global motionMode;
motionMode = 'J';



function LinearMode_Callback(hObject, eventdata, handles)
% --- Executes on button press in LinearMode.
% This function sends a message to move the robot in linear mode.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to LinearMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LinearMode
global motionMode;
motionMode = 'L';



function currentX_Callback(hObject, eventdata, handles)
% hObject    handle to currentX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentX as text
%        str2double(get(hObject,'String')) returns contents of currentX as a double



function currentX_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to currentX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function currentY_Callback(hObject, eventdata, handles)
% hObject    handle to currentY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentY as text
%        str2double(get(hObject,'String')) returns contents of currentY as a double



function currentY_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to currentY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function currentZ_Callback(hObject, eventdata, handles)
% hObject    handle to currentZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentZ as text
%        str2double(get(hObject,'String')) returns contents of currentZ as a double



function currentZ_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to currentZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function currentQ1_Callback(hObject, eventdata, handles)
% hObject    handle to currentQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentQ1 as text
%        str2double(get(hObject,'String')) returns contents of currentQ1 as a double



function currentQ1_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to currentQ1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function currentQ2_Callback(hObject, eventdata, handles)
% hObject    handle to currentQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentQ2 as text
%        str2double(get(hObject,'String')) returns contents of currentQ2 as a double



function currentQ2_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to currentQ2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function currentQ3_Callback(hObject, eventdata, handles)
% hObject    handle to currentQ3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentQ3 as text
%        str2double(get(hObject,'String')) returns contents of currentQ3 as a double



function currentQ3_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to currentQ3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function currentQ4_Callback(hObject, eventdata, handles)
% hObject    handle to currentQ4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentQ4 as text
%        str2double(get(hObject,'String')) returns contents of currentQ4 as a double



function currentQ4_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to currentQ4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function setX_Callback(hObject, eventdata, handles)
% --- Executes on button press in setX.
% This function sends a command to move the robot to a specified location.
% The destination coordinates are given by the x, y, and z values specified
% by the user.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to setX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Xpos;
Xpos = get(handles.currentX, 'String');
global Ypos;
Ypos = get(handles.currentY, 'String');
global Zpos;
Zpos = get(handles.currentZ, 'String');
global motionMode;
string = sprintf(['M' motionMode '[%s,%s,%s]'], Xpos, Ypos, Zpos);
SendCommand(string);


function setY_Callback(hObject, eventdata, handles)
% --- Executes on button press in setY.
% This function sends a command to move the robot to a specified location.
% The destination coordinates are given by the x, y, and z values specified
% by the user.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to setY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Xpos;
Xpos = get(handles.currentX, 'String');
global Ypos;
Ypos = get(handles.currentY, 'String');
global Zpos;
Zpos = get(handles.currentZ, 'String');
global motionMode;
string = sprintf(['M' motionMode '[%s,%s,%s]'], Xpos, Ypos, Zpos);
SendCommand(string);


function setZ_Callback(hObject, eventdata, handles)
% --- Executes on button press in setZ.
% This function sends a command to move the robot to a specified location.
% The destination coordinates are given by the x, y, and z values specified
% by the user.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to setZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Xpos;
Xpos = get(handles.currentX, 'String');
global Ypos;
Ypos = get(handles.currentY, 'String');
global Zpos;
Zpos = get(handles.currentZ, 'String');
global motionMode;
string = sprintf(['M' motionMode '[%s,%s,%s]'], Xpos, Ypos, Zpos);
SendCommand(string);



function vacpump_Callback(hObject, eventdata, handles)
% --- Executes on button press in vacpump.
% This function sends a  message to set the value of the Vacuum Pump output
% based on the user click on the toggle button.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to vacpump (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
t = get(hObject,'String');
if (length(t) == 12)
    SendCommand('IO11');
    set(hObject,'String', 'Vac Pump ON');
else
    SendCommand('IO10');
    set(hObject,'String', 'Vac Pump OFF');
end


function vacsol_Callback(hObject, eventdata, handles)
% --- Executes on button press in vacsol.
% This function sends a  message to set the value of the Vacuum Solenoid
% output based on the user click on the toggle button.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to vacsol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
t = get(hObject,'String');
if (length(t) == 11)
    SendCommand('IO21');
    set(hObject,'String', 'Vac Sol ON');
else
    SendCommand('IO20');
    set(hObject,'String', 'Vac Sol OFF');
end


function conrun_Callback(hObject, eventdata, handles)
% --- Executes on button press in conrun.
% This function sends a  message to set the value of the Conveyor Run
% output based on the user click on the toggle button.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to conrun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
t = get(hObject,'String');
global IOReceived;
if (length(t) == 11) && IOReceived(5) == 1
    SendCommand('IO31');
    set(hObject,'String', 'Con Run ON');
else%if IOReceived(5) == 1
    SendCommand('IO30');
    set(hObject,'String', 'Con Run OFF');
end


function condir_Callback(hObject, eventdata, handles)
% --- Executes on button press in condir.
% This function sends a  message to set the value of the Conveyor Direction
% output based on the user click on the toggle button.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to condir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
t = get(hObject,'String');
if (length(t) == 11)
    SendCommand('IO41');
    set(hObject,'String', 'Con Dir Away');
else
    SendCommand('IO40');
    set(hObject,'String', 'Con Dir Tow');
end


function qwirkleCheck_Callback(hObject, eventdata, handles)
% --- Executes on button press in qwirkleCheck.
% This function runs the block detection algorithm from assignment 1 of
% MTRN4230 on the camera feed.
% The inputs are the object raising the event, the event data, and the
% handles object. The output is shown on the command line.
% hObject    handle to qwirkleCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
QwirkleBlockDetection(handles);

function QwirkleBlockDetection(handles)
global TableVidFeed;
global QwirkleInfo;
tableImage = getsnapshot(TableVidFeed);

% Put qwirkle block detection code in the following function:
QwirkleInfo = Qwirkle(tableImage);

function showLindt_Callback(hObject, eventdata, handles)
% --- Executes on button press in showLindt.
% This function runs an algorithm to check the number of lindt boxes in the
% camera feed.
% The inputs are the object raising the event, the event data, and the
% handles object. The output is shown on the command line.
% hObject    handle to showLindt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ConveyorVidFeed;
conveyorImage = getsnapshot(ConveyorVidFeed);
disp('Finding delicious expired chocolate...');
disp(detect_choc(conveyorImage,2));


function currentSpeed_Callback(hObject, eventdata, handles)
% hObject    handle to currentSpeed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currentSpeed as text
%        str2double(get(hObject,'String')) returns contents of currentSpeed as a double



function currentSpeed_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to currentSpeed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function setSpeed_Callback(hObject, eventdata, handles)
% --- Executes on button press in setSpeed.
% This function sets the speed of the robot.
% The inputs are the object raising the event, the event data, and the
% handles object. Theis no output.
% hObject    handle to setSpeed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Speed = uint16(str2double(get(handles.currentSpeed, 'String')));
Speed = min(100, Speed);
set(handles.currentSpeed, 'String', num2str(Speed));
s = (Speed * 67) + 300;
string = sprintf('SP%s', num2str(s));
SendCommand(string);



function plusXLinear_Callback(hObject, eventdata, handles)
% --- Executes on button press in plusXLinear.
% This function sends a command to move the robot in positive X direction
% by the amount specified by the linear increment.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to plusXLinear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SendCommand('LJX+');


function plusYLinear_Callback(hObject, eventdata, handles)
% --- Executes on button press in plusYLinear.
% This function sends a command to move the robot in positive Y direction
% by the amount specified by the linear increment.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to plusYLinear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SendCommand('LJY+');


function plusZLinear_Callback(hObject, eventdata, handles)
% --- Executes on button press in plusZLinear.
% This function sends a command to move the robot in positive Z direction
% by the amount specified by the linear increment.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to plusZLinear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SendCommand('LJZ+');


function minusXLinear_Callback(hObject, eventdata, handles)
% --- Executes on button press in minusXLinear.
% This function sends a command to move the robot in negative X direction
% by the amount specified by the linear increment.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to minusXLinear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SendCommand('LJX-');


function minusYLinear_Callback(hObject, eventdata, handles)
% --- Executes on button press in minusYLinear.
% This function sends a command to move the robot in negative Y direction
% by the amount specified by the linear increment.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to minusYLinear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SendCommand('LJY-');


function minusZLinear_Callback(hObject, eventdata, handles)
% --- Executes on button press in minusZLinear.
% This function sends a command to move the robot in negative Z direction
% by the amount specified by the linear increment.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to minusZLinear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SendCommand('LJZ-');


function incrementSizeNum_Callback(hObject, eventdata, handles)
% --- Executes on selection change in incrementSizeNum.
% This function sends a command to set the linear increment.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to incrementSizeNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns incrementSizeNum contents as cell array
%        contents{get(hObject,'Value')} returns selected item from incrementSizeNum
incrementSizeNum_contents = cellstr(get(hObject,'String'));
selected_item = incrementSizeNum_contents{get(hObject,'Value')};

switch selected_item
    case '10'
        SendCommand('LI010');
    case '50'
        SendCommand('LI050');
    case '100'
        SendCommand('LI100');
end


function incrementSizeNum_CreateFcn(hObject, eventdata, handles)
% --- Executes during object creation, after setting all properties.
% hObject    handle to incrementSizeNum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function plusOne_Callback(hObject, eventdata, handles)
% --- Executes on button press in plusOne.
% This function sends a command to move the joint specified by the user in
% the positive direction by 1 unit.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to plusOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Joint;
string = sprintf('%s+1', Joint);
SendCommand(string);


function minusOne_Callback(hObject, eventdata, handles)
% --- Executes on button press in minusOne.
% This function sends a command to move the joint specified by the user in
% the negative direction by 1 unit.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to minusOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Joint;
string = sprintf('%s-1', Joint);
SendCommand(string);


function plusTen_Callback(hObject, eventdata, handles)
% --- Executes on button press in plusTen.
% This function sends a command to move the joint specified by the user in
% the positive direction by 10 units.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to plusTen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Joint;
string = sprintf('%s+0', Joint);
SendCommand(string);


function minusTen_Callback(hObject, eventdata, handles)
% --- Executes on button press in minusTen.
% This function sends a command to move the joint specified by the user in
% the negative direction by 10 unit.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to minusTen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Joint;
string = sprintf('%s-0', Joint);
SendCommand(string);


function pauseButton_Callback(hObject, eventdata, handles)
% --- Executes on button press in pauseButton.
% This function sends a command to pause the motion of the robot.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to pauseButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
SendCommand('PM');



function connectButton_Callback(hObject, eventdata, handles)
% --- Executes on button press in connectButton.
% This function attempts to reconnect to RAPID. It does so by closing the
% existing connections and attempting to connect to RAPID.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
if ~exist('checkFlag','var')
    try
        fclose(instrfind);
    catch
    end
    % Clear global variables
    clearvars -global
    checkFlag = 1;
    % Set global variable for number of times MainControl has attempted
    % to read from the receive socket
    global ReceiveCount;
    ReceiveCount = 0;
    
    global motionMode;
    motionMode = 'J';
    
    % Start logs, sockets, timer, and camera
    StartCommunicationBox(handles);
    StartSockets(handles);
    StartTimer(handles);
    StartCameras(handles);
    StartQwirkleTimer(handles);
    % hObject    handle to connectButton (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
end



function j2Select_Callback(hObject, eventdata, handles)
% --- Executes on button press in j2Select.
% This function selects joint 2 for joint motion mode.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to j2Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of j2Select
global Joint;
Joint = 'JJ2';


function j3Select_Callback(hObject, eventdata, handles)
% --- Executes on button press in j3Select.
% This function selects joint 3 for joint motion mode.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to j3Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of j3Select
global Joint;
Joint = 'JJ3';


function j4Select_Callback(hObject, eventdata, handles)
% --- Executes on button press in j4Select.
% This function selects joint 4 for joint motion mode.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to j4Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of j4Select
global Joint;
Joint = 'JJ4';

function j5Select_Callback(hObject, eventdata, handles)
% --- Executes on button press in j5Select.
% This function selects joint 5 for joint motion mode.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to j5Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of j5Select
global Joint;
Joint = 'JJ5';

function j6Select_Callback(hObject, eventdata, handles)
% --- Executes on button press in j6Select.
% This function selects joint 6 for joint motion mode.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to j6Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of j6Select
global Joint;
Joint = 'JJ6';

function j1Select_Callback(hObject, eventdata, handles)
% --- Executes on button press in j1Select.
% This function selects joint 1 for joint motion mode.
% The inputs are the object raising the event, the event data, and the
% handles object. There is no output.
% hObject    handle to j1Select (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of j1Select
global Joint;
Joint = 'JJ1';


% --- Executes on key press with focus on clear and none of its controls.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

numBlocks=5;

x=[180,190,200,210,220];
y=[0,0,0,0,0];
z=[175,175,175,175,175];

xbox=[0,30];
ybox=[290,400];
xmin=xbox(1);
ymin=ybox(1);

%turn on vac
SendCommand('IO11');

for i=1:numBlocks
    string=sprintf('MJ[%d,%d,%d]',x(i),y(i),z(i)-10)
    SendCommand(string);
    SendCommand('IO21');
    %move arm above conveyor position
    string1=sprintf('MJ[%d,%d,%d]',xmin,ymin,z(i)+20);
    SendCommand(string1);
    %lower arm
    string2=sprintf('MJ[%d,%d,%d]',xmin,ymin,50);
    SendCommand(string2);
    SendCommand('IO20');
    %lift the arm to avoid collision
    string3=sprintf('MJ[%d,%d,%d]',xmin,ymin,175);
    SendCommand(string3);
    %offset for box
    xmin=xmin+15;
end

%turn off vac
SendCommand('IO10');
